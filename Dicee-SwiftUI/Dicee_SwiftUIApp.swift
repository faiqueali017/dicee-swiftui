//
//  Dicee_SwiftUIApp.swift
//  Dicee-SwiftUI
//
//  Created by Faiq on 14/02/2021.
//

import SwiftUI

@main
struct Dicee_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
